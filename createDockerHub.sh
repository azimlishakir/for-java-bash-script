#!/bin/bash

FILE=Dockerfile
PORT=8080

username=$1
repo=$2
date=$(date +"%H_%M")
tag=$( docker images | head -n2 | awk '{print $3}' | tail -n1  )



function writeData(){
echo "FROM openjdk:8-jdk-alpine
ADD target/*.jar *.jar
EXPOSE $PORT
ENTRYPOINT [\"java\", \"-jar\", \"*.jar\"]
" > $FILE
}

function dockerPushRepo(){
  docker build -t $username/$repo:$date .
  docker tag $tag $username/$repo:$date
  docker push $username/$repo:$date
}


if test -f "$FILE"
then
   echo "Bele file artiq var"
   dockerPushRepo
   
else
  touch $FILE
  echo "$FILE yaradildi"
  writeData
  dockerPushRepo
fi
